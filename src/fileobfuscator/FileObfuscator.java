package fileobfuscator;

import java.io.IOException;
import java.util.Scanner;

/**
 * Класс для реализации мини-обфускатора
 *
 */
public class FileObfuscator {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        System.out.println("Введите путь к файлу:");
        String startWay = scanner.nextLine(); //"src\\bad\\com\\patterns\\obfuscator\\Main.java";

        System.out.println("Введите путь куда сохранить файл:");
        String finWay = scanner.nextLine(); //"src\\bad\\com\\patterns\\obfuscator\\NewMain.java";

        FacadeObfuscate facadeObfuscate = new FacadeObfuscate(startWay, finWay);
        facadeObfuscate.obfuscate();
    }
}